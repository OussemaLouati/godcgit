FROM golang:1.15-alpine AS builder
RUN apk add build-base
RUN mkdir /app

ADD . /app

WORKDIR /app
COPY go.mod . 
COPY go.sum .

RUN go mod download

RUN CGO_ENABLED=1 GOOS=linux go build -o main .

FROM alpine:latest AS production 
COPY --from=builder /app . 
RUN adduser -D myuser
USER myuser
CMD ["./main"]

